package tree.traversal.binaryTree.BFS_DFS;

import tree.traversal.model.TreeNode;

import java.util.Stack;

/**
 * Approach is quite simple, use Stack.
 First add the add root to the Stack.
 Pop out an element from Stack and add its right and left children to stack.
 Pop out an element and print it and add its children.
 Repeat the above two steps until the Stack id empty.
 */
public class DepthFirstSearch {

    public static void main(String []s){

        TreeNode root = new TreeNode(1);
        root.left = new TreeNode(2);
        root.left.left = new TreeNode(4);
        root.left.right = new TreeNode(5);
        root.right = new TreeNode(3);
        root.right.left = new TreeNode(6);
        root.right.right = new TreeNode(7);
        DepthFirstSearch depthFirstSearch = new DepthFirstSearch();
        depthFirstSearch.traversal(root);

    }

    public void traversal(TreeNode node) {
        Stack<TreeNode> stack = new Stack<>();
        stack.push(node);
        while (!stack.isEmpty()){
            TreeNode temp = stack.pop();
            if(temp.right!=null){
                stack.push(temp.right);
            }
            if(temp.left!=null){
                stack.push(temp.left);
            }
            System.out.print(temp.data+" ");
        }
    }
}

