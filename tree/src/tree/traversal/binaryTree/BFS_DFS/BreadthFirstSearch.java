package tree.traversal.binaryTree.BFS_DFS;

import tree.traversal.model.TreeNode;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Take a Empty Queue.
 Start from the root, insert the root into the Queue.
 Now while Queue is not empty,
 Extract the node from the Queue and insert all its children into the Queue.
 Print the extracted node.
 */
public class BreadthFirstSearch {

    public static void main(String[] s){
        BreadthFirstSearch treeSearch = new BreadthFirstSearch();

        TreeNode root = new TreeNode(5);
        root.left = new TreeNode(10);
        root.right = new TreeNode(15);
        root.left.left = new TreeNode(20);
        root.left.right = new TreeNode(25);
        root.right.left = new TreeNode(30);
        root.right.right = new TreeNode(35);
        treeSearch.levelOrderTraverse(root);
    }

    public void levelOrderTraverse(TreeNode node){
        Queue<TreeNode> queue = new LinkedList<>();
        if(node == null) {
            return;
        }
        queue.add(node);
        while(!queue.isEmpty()){
            TreeNode temp = queue.remove();
            System.out.print(temp.data+" ");
            if(temp.left!=null){
                queue.add(temp.left);
            }
            if(temp.right!=null){
                queue.add(temp.right);
            }
        }

    }
}
