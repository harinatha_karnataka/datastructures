package tree.traversal.orders;

import tree.traversal.model.TreeNode;

import java.util.Stack;

public class PreorderInorderPostorder {

    public static void main(String []s){
        TreeNode root = new TreeNode(10);
        root.left = new TreeNode(6);
        root.right = new TreeNode(21);
        root.left.left = new TreeNode(1);
        root.left.right = new TreeNode(8);
        root.right.left = new TreeNode(13);
        root.right.right = new TreeNode(25);
        root.right.left.left = new TreeNode (12);
        root.right.left.right = new TreeNode(18);

        PreorderInorderPostorder treeOrder = new PreorderInorderPostorder();

        //Pre-order traversal
        System.out.print("Pre Order Recursion  ");
        treeOrder.preOrder(root);
        System.out.println();
        System.out.print("Pre Order Iteration  ");
        treeOrder.preOrderIteration(root);
        System.out.println();

        //In order traversal
        System.out.print("In Order Recursion  ");
        treeOrder.inOrder(root);
        System.out.println();
        System.out.print("In Order Iteration  ");
        //in order iteration
        treeOrder.inorderIteration(root);
        System.out.println();

        //Post-order traversal
        System.out.print("Post Order Recursion  ");
        treeOrder.postOrder(root);
        System.out.println();
        System.out.print("Post Order Recursion  ");
        treeOrder.postOrderIteration(root);
    }

    public void inOrder(TreeNode node){

        if(node == null){
            return;
        }
        inOrder(node.left);
        System.out.print(node.data+" ");
        inOrder(node.right);
    }

    public void inorderIteration(TreeNode node){
        Stack<TreeNode> stack = new Stack();
        while(true){
            // Go to the left extreme insert all the elements to stack
            while(node !=null){
                stack.push(node);
                node=node.left;
            }
            if(stack.isEmpty()){
                return;
            }
            // pop the element from the stack , print it and add the nodes at
            // the right to the Stack
            node = stack.pop();
            System.out.print(node.data+" ");
            node=node.right;
        }
    }

    public void preOrder(TreeNode node){
        if(node == null){
            return;
        }
        System.out.print(node.data+" ");
        preOrder(node.left);
        preOrder(node.right);
    }

    public  void preOrderIteration(TreeNode node){
        Stack<TreeNode> stack = new Stack();
        while(true){
            // First print the root node and then add left node
            while(node !=null){
                System.out.print(node.data+" ");
                stack.push(node);
                node=node.left;
            }
            // check if Stack is emtpy, if yes, exit from everywhere
            if(stack.isEmpty()){
                return;
            }
            // pop the element from the stack , print it and add the nodes at
            // the right to the Stack
            node = stack.pop();

            node=node.right;
        }
    }
    public void postOrder(TreeNode node){
        if(node == null){
            return;
        }
        postOrder(node.left);
        postOrder(node.right);
        System.out.print(node.data+" ");
    }

    public void postOrderIteration(TreeNode node){
        Stack<TreeNode> stack1 = new Stack<>();
        Stack<TreeNode> stack2 = new Stack<>();
        // push the root node into first stack.
        stack1.push(node);
        while(stack1.isEmpty() == false){
            // take out the root and insert into second stack.
            TreeNode temp = stack1.pop();
            stack2.push(temp);
            // now we have the root, push the left and right child of root into
            // the first stack.
            if(temp.left!=null){
                stack1.push(temp.left);
            }
            if(temp.right!=null){
                stack1.push(temp.right);
            }
        }
        //once the all node are traversed, take out the nodes from second stack and print it.
        while (stack2.isEmpty()==false){
            System.out.print(stack2.pop().data+" ");
        }
    }
}
