package sort;

public class MergeSort {

	static int[]a = {1,5,3,4,2};
	static int[] b = new int[a.length];
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		mergeSort(0, a.length-1);
		
		printArray(a);
	}

	private static void printArray(int a[])
	{
		for(int i=0;i<a.length;i++)
		{
			System.out.print(a[i]+ "  ");
		}
	}

	/**
	 *
	 * @param low
	 * @param high
	 */
	private static void mergeSort(int low,int high)
	{
		if(low<high)
		{
			int middle = low+(high-low)/2;
			mergeSort(low, middle);
			mergeSort(middle+1,high);
			merge(low,middle,high);
		}
	}

	/**
	 * Have we reached the end of any of the arrays?
	 No:
	 Compare current elements of both arrays
	 Copy smaller element into sorted array
	 Move pointer of element containing smaller element
	 Yes:
	 Copy all remaining elements of non-empty array
	 * @param low
	 * @param mid
	 * @param high
	 */
	private static void merge(int low,int mid,int high)
	{
		for(int i=low;i<=high;i++)
		{
			b[i]=a[i];	
		}
		int i=low;
		int j=mid+1;
		int k=low;
		
		
		while(i<=mid && j<= high)
		{
			if(b[i]<=b[j])
			{
				a[k]=b[i];
				i++;
			}
			else
			{
				a[k]=b[j];
				j++;
			}
			k++;
		}
		while(i<=mid)
		{
			a[k]=b[i];
			k++;
			i++;
		}
		while(j<=high)
		{
			a[k]=b[j];
			k++;
			j++;
		}
	}
}
