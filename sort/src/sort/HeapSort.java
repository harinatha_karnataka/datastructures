package sort;

/**
 * https://www.programiz.com/dsa/heap-sort
 */
/**
 * What is Heap Data Structure ?
 Heap is a special tree-based data structure. A binary tree is said to follow a heap data structure if

 it is a complete binary tree
 All nodes in the tree follow the property that they are greater than their children i.e.
 the largest element is at the root and both its children and smaller than the root and so on.
 Such a heap is called a max-heap. If instead all nodes are smaller than their children,
 it is called a min-heap
 */

/**
 * Procedures to follow for Heapsort
 Since the tree satisfies Max-Heap property, then the largest item is stored at the root node.
 Remove the root element and put at the end of the array (nth position)
 Put the last item of the tree (heap) at the vacant place.
 Reduce the size of the heap by 1 and heapify the root element again so that we have highest element at root.
 The process is repeated until all the items of the list is sorted.
 */
public class HeapSort {

    public void sort(int arr[]){
        int n = arr.length;
        /**
         * In the case of complete tree, the first index of non-leaf node is given by n/2 - 1.
         * All other nodes after that are leaf-nodes and thus don’t need to be heapified.
         */
            //Build max heap
        for(int i=n/2-1;i>=0;i--){
            heapify(arr,n,i);
        }

        //heap sort
        for (int i=n-1;i>=0;i--){
            int temp = arr[0];
            arr[0]=arr[i];
            arr[i]=temp;
            //heapify root element
            heapify(arr,i,0);
        }
    }

    /**
     * Thus, to maintain the max-heap property in a tree where both sub-trees are max-heaps,
     * we need to run heapify on the root element repeatedly until it is larger than its children or
     * it becomes a leaf node.

     We can combine both these conditions in one heapify function as
     * @param arr
     * @param n
     * @param i
     */

    public void heapify(int arr[],int n,int i){
        // Find largest among root, left child and right child
        int largest = i;
        int l = 2*i + 1;
        int r = 2*i + 2;

        if(l<n && arr[l] > arr[largest]){
            largest = l;
        }
        if(r<n && arr[r] > arr[largest]){
            largest = r;
        }
        // Swap and continue heapifying if root is not largest
        if(largest != i){
            int temp = arr[i];
            arr[i] = arr[largest];
            arr[largest] = temp;
            heapify(arr,n,largest);
        }
    }

    static void printArray(int arr[])
    {
        int n = arr.length;
        for (int i=0; i < n; ++i)
            System.out.print(arr[i]+" ");
        System.out.println();
    }

    public static void main(String args[])
    {
        int arr[] = {1,12,9,5,6,10};
        printArray(arr);
        HeapSort hs = new HeapSort();
        hs.sort(arr);

        System.out.println("Sorted array is");
        printArray(arr);
    }
}
