package sort;

import java.util.ArrayList;

//average case - o(nlogn)
//worst case - O(n^2)
public class QuickSort {
    public static void main(String[] args){
        QuickSort obj = new QuickSort();
        ArrayList<Integer> list = new ArrayList<>();
        list.add(5);
        list.add(8);
        list.add(1);
        list.add(3);
        list.add(9);
        list.add(6);
        System.out.println("Before sort "+list);
obj.quickSort(list,0,list.size()-1);
        System.out.println("After sort"+list);
    }

    public void quickSort(ArrayList<Integer> list ,int low,int high){
        int i=low , j=high;
        int pivot = list.get((low + (high - low)/2));
        while (i <= j){

            while (list.get(i)<pivot){
                i++;
            }
            while(list.get(j)>pivot)
            {
                j--;
            }
            if(i<=j){
                swap(list,i,j);
                i++;
                j--;
            }
        }
        if(low<j){
            quickSort(list,low,j);
        }
        if(i<high){
            quickSort(list,i,high);
        }
    }

    private void swap(ArrayList<Integer> list,int i,int j){
        int temp = list.get(j);
        list.set(j,list.get(i));
        list.set(i,temp);
    }
}
