package sort;

import java.util.ArrayList;

// All items to the left are smaller
// o(n^2)
/**
 * Explanation

 Suppose, you want to sort elements in ascending as in above figure. Then,

 Step 1: The second element of an array is compared with the elements that appears before
 it (only first element in this case). If the second element is smaller than first element,
 second element is inserted in the position of first element. After first step, first two elements of an array
 will be sorted.
 Step 2: The third element of an array is compared with the elements that appears before
 it (first and second element). If third element is smaller than first element, it is inserted in the
 position of first element. If third element is larger than first element but, smaller than second element,
 it is inserted in the position of second element. If third element is larger than both the elements,
 it is kept in the position as it is. After second step, first three elements of an array will be sorted.
 Step 3: Similary, the fourth element of an array is compared with the elements that appears before
 it (first, second and third element) and the same procedure is applied and that element is inserted in the
 proper position. After third step, first four elements of an array will be sorted.
 */
public class InsertionSort {

    public static void main(String[] args){
        InsertionSort obj = new InsertionSort();
        ArrayList<Integer> list = new ArrayList<>();
        list.add(5);
        list.add(8);
        list.add(1);
        list.add(3);
        list.add(9);
        list.add(6);
        System.out.println("Before sort "+list);
        obj.insertionSort(list);
        System.out.println("After sort"+list);
    }

    public ArrayList insertionSort(ArrayList<Integer> list){
        int i,j,key,temp;
        for(i=1;i<list.size();i++){
            key=list.get(i);
            j=i-1;
            while(j>=0&& key<list.get(j)){
                //swap
                temp=list.get(j);
                list.set(j,list.get(j+1));
                list.set(j+1,temp);
                j--;
            }
        }
        return list;
    }
}
