package sort;

import java.util.ArrayList;


public class SelectionSort {

    public static void main(String[] args){
        SelectionSort obj = new SelectionSort();
        ArrayList<Integer> list = new ArrayList<>();
        list.add(5);
        list.add(8);
        list.add(1);
        list.add(3);
        list.add(9);
        list.add(6);
        System.out.println("Before sort "+list);
        obj.selectionSort(list);
        System.out.println("After sort"+list);
    }

    public void selectionSort(ArrayList<Integer> list){
        int i,j,minValue,minIndex,temp=0;
        for(i=0;i<list.size();i++){
            minValue=list.get(i);
            minIndex=i;
            for(j=i+1;j<list.size();j++){
                if(list.get(j)<minValue){
                    minValue=list.get(j);
                    minIndex=j;
                }
            }
            if(minValue<list.get(i)){
                temp=list.get(i);
                list.set(i,list.get(minIndex));
                list.set(minIndex,temp);
            }
        }
    }
}
